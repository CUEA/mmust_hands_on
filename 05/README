*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.)

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING:
Version of Siesta: siesta-3.0-b (serial mode)
Compiler: g95 (version 0.92)
Compilation flags: FFLAGS= -O2 -Wall
                   FPPFLAGS= -DGRID_DP $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
Double precision for the grid variables enabled.
No linear algebra libraries used.
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS

*******************************************************************************

All the lines after the $ should be written by the user in the command line.


* Edit the input file, SrTiO3.fdf, and study the different variables.
Pay special attention to the new variables in this example, and
check their meaning in the User's Guide:
   %block ProjectedDensityOfStates
   %block PDOS.kgrid_Monkhorst_Pack

* Select the lattice constant wich minimizes the total energy of the
system (in this case, you do not have to compute it; it is given to 
you and amounts to 3.874 Ang). Run the code setting the right lattice
constant in the input.
Once the input is ready, run a command like this:

$ siesta < SrTiO3.fdf > SrTiO3.out 

* Siesta writes the Density Of States (DOS), 
and the Projected Density Of States (PDOS) information in 
two files called SystemLabel.DOS, and SystemLabel.PDOS, respectively.
Take a look at these file with a text editor.
The meaning of the different columns and the information
contained in them is explained in the 
Exercise-PDOS-SrTiO3.pdf file that comes with this exercise.

* To plot the DOS, simlpy type
$ gnuplot
$ gnuplot> plot "SrTiO3.DOS" using 1:2 with lines

The result is a plot with the DOS of SrTiO3.

To focus only on the top of the valence band, from gnuplot type
$ gnuplot> set xrange [-25.0:0.0]  # this is large enough to include 
                                   # O 2s and Ba 5p character bands
$ gnuplot> replot

* To produce a postscript file with one of the previous figures
First the number of these points, and then in two columns, the 

gnuplot> set terminal postscript color
gnuplot> set output "SrTiO3.DOS.ps"
gnuplot> replot
gnuplot> quit

* To generate a pdf file from the previous postscript

$ ps2pdf SrTiO3.DOS.ps


* To plot the PDOS, we have to use the fmpdos utility program
implemented by Andrei Postnikov.

It is included in the directory Util/Contrib/Apostnikov.

To compile it, go to this directory and type:

$ make

Then go to the directory where you have run the simulation
and run it. Simply answer the questions at run-time
(see the examples in the pdf file explaining this exercise.

You can project on all the atomic orbitals of a given atom,
or only on some subset of orbitals, whose indices are
fully explained in the file SystemLabel.ORB_INDX
